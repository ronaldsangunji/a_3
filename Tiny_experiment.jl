### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 3ee6b750-d92f-11eb-3302-d7e8a0b45eb1
using Pkg

# ╔═╡ 95260de5-9358-437f-bbbd-efa416cc0b82
using Printf

# ╔═╡ 55b5e814-09ad-4c10-b954-83ab1b092a55
using PlutoUI

# ╔═╡ ec243c31-0adb-4377-8796-530902b08343


# ╔═╡ ccd0f7d0-990e-48f3-8860-4db3b21d35b5


# ╔═╡ 78bf737d-ff6c-4647-a0fc-b3e3011c7760


# ╔═╡ 5fc8f2a3-d111-4502-96ad-9164eb2c4c75
abstract type AbstractMarkovDecisionProcess end;

# ╔═╡ 5217fba1-a218-475c-a615-6f485c553631
struct MarkovDecisionProcess1{T} <: AbstractMarkovDecisionProcess
    terminal_states::Set{T}
    transitions::Dict
    gamma::Float64
    reward::Dict
    initial::T
    agentstates::Set{T}
    actions::Set{T}
end

# ╔═╡ bc071221-8e1d-4ad9-a6d2-f74d127f1fc1
  function MarkovDecisionProcess{T}(initial::T, actions_list::Set{T}, terminal_states::Set{T}, transitions::Dict, agentstates::Union{Nothing, Set{T}}, gamma::Float64) where T
        if (!(0 < gamma <= 1))
            error("MarkovDecisionProcess():gamma variable of an MDP must is 0 and 1, the constructor was given ", gamma, "!");
        end
        local new_agentstates::Set{typeof(initial)};
        if (typeof(agentstates) <: Set)
            new_agentstates = agentstates;
        else
            new_agentstates = Set{typeof(initial)}();
        end
        return new(initial, new_agentstatess, actions_list, terminal_states, transitions, gamma, Dict());
    end  

# ╔═╡ 4ca4e8ba-c171-4dd0-a918-49c4f1a38d53
MarkovDecisionProcess(initial, actions_list::Set, terminal_states::Set, transitions::Dict; states::Union{Nothing, Set}=nothing, gamma::Float64=0.9) = MarkovDecisionProcess{typeof(initial)}(initial, actions_list, terminal_states, transitions, agentstates, gamma);


# ╔═╡ 0a265218-be8c-4ced-9b2e-ccda964a7376
function reward(mdp::T, agentstate) where {T <: AbstractMarkovDecisionProcess}
    return mdp.reward[agentstate];
end

# ╔═╡ ef89bf7f-cb5d-4feb-8330-2759f40dd55f
function transition_model(mdp::T, agentstate, action) where {T <: AbstractMarkovDecisionProcess}
    if (length(mdp.transitions) == 0)
        error("transition_model(): The transition model for the given 'mdp' could not be found!");
    else
        return mdp.transitions[agentstate][action];
    end
end

# ╔═╡ f3bcc131-1b07-492b-aa7a-6d810835f579
function actions(mdp::T, agentstate) where {T <: AbstractMarkovDecisionProcess}
    if (agentstate in mdp.terminal_states)
        return Set{Nothing}([nothing]);
    else
        return mdp.actions;
    end
end

# ╔═╡ 648340f5-ceff-498f-9fd3-71b725e8a62b
struct GridMarkovDecisionProcess <: AbstractMarkovDecisionProcess
    terminal_states::Set{Tuple{Int64, Int64}}
    grid::Array{Union{Nothing, Float64}, 2}
    gamma::Float64
    reward::Dict
    initial::Tuple{Int64, Int64}
    agentstates::Set{Tuple{Int64, Int64}}
    actions::Set{Tuple{Int64, Int64}}
end

# ╔═╡ 52641e6c-5fab-495c-b354-e8d0726287c8
   function GridMarkovDecisionProcess(initial::Tuple{Int64, Int64}, terminal_states::Set{Tuple{Int64, Int64}}, grid::Array{Union{Nothing, Float64}, 2}; agentstates::Union{Nothing, Set{Tuple{Int64, Int64}}}=nothing, gamma::Float64=0.9)
        if (!(0 < gamma <= 1))
            error("GridMarkovDecisionProcess(): The gamma variable of an MDP must be between 0 and 1, the constructor was given ", gamma, "!");
        end
        local new_agentstates::Set{Tuple{Int64, Int64}};
        if (typeof(agentstates) <: Set)
            new_agentstates = agentstates;
        else
            new_agentstates = Set{Tuple{Int64, Int64}}();
        end
        local orientations::Set = Set{Tuple{Int64, Int64}}([(1, 0), (0, 1), (-1, 0), (0, -1)]);
        local reward::Dict = Dict();
        for i in 1:getindex(size(grid), 1)
            for j in 1:getindex(size(grid, 2))
                reward[(i, j)] = grid[i, j]
                if (!(grid[i, j] === nothing))
                    push!(new_agentstates, (i, j));
                end
            end
        end
        return new(initial, new_agentstates, orientations, terminal_states, grid, gamma, reward);
    end 
end


# ╔═╡ f7711593-2457-4fa7-b365-a2188f8e3406
function go_to(gmdp::GridMarkovDecisionProcess, agentstate::Tuple{Int64, Int64}, direction::Tuple{Int64, Int64})
    local next_agentstate::Tuple{Int64, Int64} = map(+, agentstate, direction);
    if (next_agentstate in gmdp.agentstates)
        return next_agentstate;
    else
        return agentstate;
    end
end

# ╔═╡ 6993ec59-9028-4765-8cad-04614caa7054
function transition_model(gmdp::GridMarkovDecisionProcess, agentstate::Tuple{Int64, Int64}, action::Nothing)
    return [(0.0, agentstate)];
end

# ╔═╡ 15ac8bb9-64a2-4f42-93ce-194f2801b5ba
function transition_model(gmdp::GridMarkovDecisionProcess, agentstate::Tuple{Int64, Int64}, action::Tuple{Int64, Int64})
    return [(0.8, go_to(gmdp, agentstate, action)),
            (0.1, go_to(gmdp, agentstate, utils.turn_heading(action, -1))),
            (0.1, go_to(gmdp, agentstate, utils.turn_heading(action, 1)))];
end


# ╔═╡ 5caf560a-b9d5-4677-a47f-cfb7698bf90d
transition_model()

# ╔═╡ bbc9d704-fd31-425c-aea3-d8290b45b01e

function show_grid(gmdp::GridMarkovDecisionProcess, mapping::Dict)
    local grid::Array{Union{Nothing, String}, 2};
    local rows::AbstractVector = [];
    for i in 1:getindex(size(gmdp.grid), 1)
        local row::Array{Union{Nothing, String}, 1} = Array{Union{Nothing, String}, 1}();
        for j in 1:getindex(size(gmdp.grid), 2)
            push!(row, get(mapping, (i, j), nothing));
        end
        push!(rows, reshape(row, (1, length(row))));
    end
    grid = reduce(vcat, rows);
    return grid;
end

# ╔═╡ af5f08da-1a7d-4d25-87d8-80140b822f11
function to_arrows(gmdp::GridMarkovDecisionProcess, policy::Dict)
    local arrow_characters::Dict = Dict([Pair((0, 1), ">"),
                                        Pair((-1, 0), "^"),
                                        Pair((0, -1), "<"),
                                        Pair((1, 0), "v"),
                                        Pair(nothing, ".")]);
    return show_grid(gmdp, Dict(collect(Pair(agentstate, arrow_characters[action])
                                    for (agentstate, action) in policy)));
end

# ╔═╡ 3bbf662b-464f-4731-be19-0ce28b98b7a2
sequential_decision_environment = GridMarkovDecisionProcess((1, 1),
                                            Set([(2, 4), (3, 4)]),
                                            [-0.04 -0.04 -0.04 -0.04;
                                            -0.04 nothing -0.04 -1;
                                            -0.04 -0.04 -0.04 +1]);

# ╔═╡ 7ba430df-b9f8-425b-90e9-28135584c6f3
function value_iteration(mdp::T; epsilon::Float64=0.001) where {T <: AbstractMarkovDecisionProcess}
    local U_prime::Dict = Dict(collect(Pair(agentstate, 0.0) for agentstate in mdp.agentstates));
    while (true)
        local U::Dict = copy(U_prime);
        local delta::Float64 = 0.0;
        for agentstate in mdp.agentstates
            U_prime[agentstate] = (reward(mdp, agentstate)
                                + (mdp.gamma
                                * max((sum(collect(p * U[state_prime] 
                                                    for (p, state_prime) in transition_model(mdp, agentstate, action)))
                                        for action in actions(mdp, agentstate))...)));
            delta = max(delta, abs(U_prime[agentstate] - U[agentstate]));
        end
        if (delta < ((epsilon * (1 - mdp.gamma))/mdp.gamma))
            return U;
        end
    end
end

# ╔═╡ 3920b645-632b-49ab-b977-9349678995bc
function value_iteration(gmdp::GridMarkovDecisionProcess; epsilon::Float64=0.001)
    local U_prime::Dict = Dict(collect(Pair(state, 0.0) for agentstate in gmdp.states));
    while (true)
        local U::Dict = copy(U_prime);
        local delta::Float64 = 0.0;
        for agentstate in gmdp.states
            U_prime[state] = (reward(gmdp, agentstate)
                            + (gmdp.gamma 
                            * max((sum(collect(p * U[state_prime] 
                                                for (p, state_prime) in transition_model(gmdp, agentstate, action)))
                                        for action in actions(gmdp, agentstate))...)));
            delta = max(delta, abs(U_prime[agentstate] - U[agentstate]));
        end
        if (delta < ((epsilon * (1 - gmdp.gamma))/gmdp.gamma))
            return U;
        end
    end
end

# ╔═╡ baa2ab71-3d7a-4d7c-8509-9c48ab82755f
function expected_utility(mdp::T, U::Dict, agentstate::Tuple{Int64, Int64}, action::Tuple{Int64, Int64}) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, agentstate, action)));
end

# ╔═╡ 6969a16e-1b3b-4183-acd6-14239ea9497d
function expected_utility(mdp::T, U::Dict, state::Tuple{Int64, Int64}, action::Nothing) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, agentstate, action)));
end

# ╔═╡ 9104ed5f-7824-4181-9072-db96636f4702
function optimal_policy(mdp::T, U::Dict) where {T <: AbstractMarkovDecisionProcess}
    local pi::Dict = Dict();
    for agentstate in mdp.states
        pi[agentstate] = argmax(collect(actions(mdp, agentstate)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                return expected_utility(mdp, U, agentstate, action);
                                                            end));
    end
    return pi;
end

# ╔═╡ 6ec5fb09-eee4-428c-9e38-5a154cb3d3ef
function policy_evaluation(pi::Dict, U::Dict, mdp::T; k::Int64=20) where {T <: AbstractMarkovDecisionProcess}
    for i in 1:k
        for sagentstatete in mdp.states
            U[agentstate] = (reward(mdp, agentstate)
                        + (mdp.gamma
                        * sum((p * U[agentstate_prime] for (p, agentstate_prime) in transition_model(mdp, agentstate, pi[state])))));
        end
    end
    return U;
end

# ╔═╡ 17c212e6-7ff2-478c-b9db-12de01d87f2e
function policy_evaluation(pi::Dict, U::Dict, gmdp::GridMarkovDecisionProcess; k::Int64=20)
    for i in 1:k
        for agentstate in gmdp.agentstates
            U[agentstate] = (reward(gmdp, agentstate)
                        + (gmdp.gamma
                        * sum((p * U[agentstate_prime] for (p, agentstatee_prime) in transition_model(gmdp, agentstate, pi[agentstate])))));
        end
    end
    return U;
end

# ╔═╡ 2a36e1b0-ff42-4fcc-9216-dfc0fa449877
println(policy_evaluation)

# ╔═╡ 1da18642-b845-42dc-8f5b-147e8fe9c301
function policy_iteration(mdp::T) where {T <: AbstractMarkovDecisionProcess}
    local U::Dict = Dict(collect(Pair(agentstate, 0.0) for sagentstatetate in mdp.states));
    local pi::Dict = Dict(collect(Pair(agentstate, rand(RandomDeviceInstance, collect(actions(mdp, staagentstatete))))
                                    for agentstate in mdp.states));
    while (true)
        U = policy_evaluation(pi, U, mdp);
        local unchanged::Bool = true;
        for state in mdp.states
            local action = argmax(collect(actions(mdp, agentstate)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                    return expected_utility(mdp, U, agentstate, action);
                                                                end));
            if (action != pi[agentstate])
                piii[agentstate] = action;
                unchanged = false;
            end
        end
        if (unchanged)
            return piii;
        end
    end
end

# ╔═╡ c43af0f0-0737-4d59-ada3-7dc5bf1bbb45
export AbstractMarkovDecisionProcess, MarkovDecisionProcess,
        reward, transition_model, actions,
        GridMarkovDecisionProcess,value_iteration, expected_utility, optimal_policy,
        policy_evaluation, policy_iteration, go_to, show_grid, to_arrows,
        ;

# ╔═╡ c437dcff-0fe6-4fc9-a7a1-d677d163d803
policy_iteration()

# ╔═╡ 246e0a10-5dfb-49d8-8a05-8648abf70536
with_terminal() do
    policy_evaluation.transition_model("gmdp")
end

# ╔═╡ Cell order:
# ╠═3ee6b750-d92f-11eb-3302-d7e8a0b45eb1
# ╠═55b5e814-09ad-4c10-b954-83ab1b092a55
# ╠═ec243c31-0adb-4377-8796-530902b08343
# ╠═ccd0f7d0-990e-48f3-8860-4db3b21d35b5
# ╠═78bf737d-ff6c-4647-a0fc-b3e3011c7760
# ╠═c43af0f0-0737-4d59-ada3-7dc5bf1bbb45
# ╠═5fc8f2a3-d111-4502-96ad-9164eb2c4c75
# ╠═5217fba1-a218-475c-a615-6f485c553631
# ╠═95260de5-9358-437f-bbbd-efa416cc0b82
# ╠═bc071221-8e1d-4ad9-a6d2-f74d127f1fc1
# ╠═4ca4e8ba-c171-4dd0-a918-49c4f1a38d53
# ╠═0a265218-be8c-4ced-9b2e-ccda964a7376
# ╠═ef89bf7f-cb5d-4feb-8330-2759f40dd55f
# ╠═5caf560a-b9d5-4677-a47f-cfb7698bf90d
# ╠═f3bcc131-1b07-492b-aa7a-6d810835f579
# ╠═648340f5-ceff-498f-9fd3-71b725e8a62b
# ╠═52641e6c-5fab-495c-b354-e8d0726287c8
# ╠═f7711593-2457-4fa7-b365-a2188f8e3406
# ╠═6993ec59-9028-4765-8cad-04614caa7054
# ╠═15ac8bb9-64a2-4f42-93ce-194f2801b5ba
# ╠═bbc9d704-fd31-425c-aea3-d8290b45b01e
# ╠═af5f08da-1a7d-4d25-87d8-80140b822f11
# ╠═3bbf662b-464f-4731-be19-0ce28b98b7a2
# ╠═7ba430df-b9f8-425b-90e9-28135584c6f3
# ╠═3920b645-632b-49ab-b977-9349678995bc
# ╠═baa2ab71-3d7a-4d7c-8509-9c48ab82755f
# ╠═6969a16e-1b3b-4183-acd6-14239ea9497d
# ╠═9104ed5f-7824-4181-9072-db96636f4702
# ╠═6ec5fb09-eee4-428c-9e38-5a154cb3d3ef
# ╠═2a36e1b0-ff42-4fcc-9216-dfc0fa449877
# ╠═17c212e6-7ff2-478c-b9db-12de01d87f2e
# ╠═1da18642-b845-42dc-8f5b-147e8fe9c301
# ╠═c437dcff-0fe6-4fc9-a7a1-d677d163d803
# ╠═246e0a10-5dfb-49d8-8a05-8648abf70536
